
import sys
import os

def shift_phon_bord(shVal, fBeg):
    fsToChange = [fn for fn in os.listdir('.') \
					if fn.startswith(fBeg) and fn.endswith('.lab')]
    phsToChange = ['a', 'k', 's', "s'", 'r', 'n', 'm', 'v']
    for fName in fsToChange:
        fr = open(fName, 'r')
        shiftedPhs = ''
        for line in fr:
            lBord, rBord, phon = line.split() 
            if phon in phsToChange:
                lBord = str(int(lBord) - int(shVal))
                rBord = str(int(rBord) - int(shVal))
                shiftedPhs += ' '.join([lBord, rBord, phon, '\n'])
            else:
                shiftedPhs += line
        fr.close()

        fw = open(fName, 'w')
        fw.write(shiftedPhs)
        fw.close()
    

def main():

    args = sys.argv[1:]
    if not args:
        print "usage: shift_val [file_begin]";
        sys.exit(1) 

    if len(args) == 2:
        shift_phon_bord(args[0], args[1])
    elif len(args) == 1:
        shift_phon_bord(args[0], 'Str')
    
  

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    main() 
