#!c:\Python27 -tt

import sys

def split_big_text(filetxt):

  fin = open(filetxt, 'r')
  
  foutname = filetxt.split('.')[0] + '_spl'
  fout = open(foutname, 'w')
  
  wordsInLine = 7
  globCounter = 1
  for line in fin:
    words = []
    for word in line.split():
      words.append(word)
      if len(words) == wordsInLine:
        fout.write(str(globCounter) + '. ' + ' '.join(words) + '\n')
	globCounter += 1
	words = []
    if len(words) > 0:
      fout.write(str(globCounter) + '. ' + ' '.join(words) + '\n') 
      globCounter += 1
	    
  fin.close()
  fout.close()
  

# Define a main() function that prints a little greeting.
def main():
  # Get the name from the command line, using 'World' as a fallback.
  
 args = sys.argv[1:]
 if not args:
   print "usage: [bigfilename]";
   sys.exit(1) 

 split_big_text(args[0])
    
  

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  main() 
